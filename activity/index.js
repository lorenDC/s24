// S24 Activity Template:
	/*Item 1.)
		- Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)
		- Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
		*/

	// Code here:
		let num = parseInt(prompt("Input number: "));
		let getCube = [num**3]

		let [numCube] = getCube

		console.log(`The cube is ${numCube}`)

	/*Item 2.)
		- Create a variable address with a value of an array containing details of an address.
		- Destructure the array and print out a message with the full address using Template Literals.*/


	// Code here:
		let address = ["6807", "Valero St", "Makati City"]
		let [bldgNum, streetNum, city] = address

		console.log(`My full address is, ${bldgNum}, ${streetNum}, ${city}`)

	/*Item 3.)
		- Create a variable animal with a value of an object data type with different animal details as its properties.
		- Destructure the object and print out a message with the details of the animal using Template Literals.
*/
	// Code here:

		let animal = {
			type: "dog",
			color: "brown",
			size: "medium"
		}

		let {type, color, size} = animal

		console.log(`My ${type} is color ${color} and a ${size} size`)

	/*Item 4.)
		- Create an array of numbers.
		- Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.*/

	// Code here:
		const numbers = [1,2,3]
		numbers.forEach((numbers) => {
		console.log(`Display ${numbers}`)
	})

/*
	Item 5.)
		- Create a class of a Dog and a constructor that will accept a name, age and breed as its properties.
		- Create/instantiate a new object from the class Dog and console log the object.*/

	// Code here:

	class Dog {
	constructor(name, age, breed) {
		this.name = name
		this.age = age
		this.breed = breed
		}
	}

	let myDog = new Dog()
	console.log(myDog)

	myDog.name = "Murphy"
	myDog.age = "8 Months"
	myDog.breed	= "Golden Retriever"

	console.log(myDog)

	let myNewDog = new Dog("Ice", "2 years old", "Golden Retriever")
	console.log(myNewDog)
